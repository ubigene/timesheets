#!/bin/bash

# Project initialisation script

set -x

# Create virtualenv
source $(dirname $0)/env.sh
if [ ! -d "${ENV_PATH}" ]; then
    virtualenv "${ENV_PATH}"
fi 

# activate virtualenv
source "${ENV_PATH}/bin/activate"

# update pip and setuptools
pip install --upgrade pip
pip install --upgrade setuptools

# install Python dependencies
pip install -r "${PROJECT_ROOT}/requirements.txt"


