#!/bin/bash

# User interface runner script
set -x

source $(dirname $0)/env.sh

# activate virtualenv
source "$ENV_PATH/bin/activate"

# run python scripts
call_bin_script timesheets write "$@"
