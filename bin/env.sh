#!/bin/bash

# Platform detection, common environmental variables and functions

# Detect platform and set platform-specific parameters
if [ $(uname) = "Darwin" ]; then
    export IS_DARWIN="true"
    export IS_CYGWIN=""
    export PYTHON="python2.7"
elif [ $(uname -o) = "Cygwin" ]; then
    export IS_DARWIN=""
    export IS_CYGWIN="true"
    export PYTHON="python2.7"
else
    export IS_DARWIN=""
    export IS_CYGWIN=""
    export PYTHON="python2.7"
fi

# We should have Python installed everywhere, so use it to find full path
function realpath {
    "${PYTHON}" -c "import os, sys; print os.path.abspath(sys.argv[1])" "$1"
}

if [ "${IS_CYGWIN}" ]; then
    THIS_PATH=$(cygpath -ua $(dirname ${BASH_SOURCE[0]}))
     export PROJECT_ROOT=$(cygpath -ua "$THIS_PATH/..")
else
    THIS_PATH=$(realpath $(dirname ${BASH_SOURCE[0]}))
    export PROJECT_ROOT=$(realpath "$THIS_PATH/..")
fi

ENV_PATH="$PROJECT_ROOT/.env"
PY_DIR="${PROJECT_ROOT}/py"

# py runner function
function call_bin_script()
{
    "${PYTHON}" "${PY_DIR}" "$@"
}   

if [ "${IS_DARWIN}" ]; then
    TMPDIR="/tmp"       # fix for .X*-lock file locations on OS X needed for Xvfb
    # add project folder to PATH for FF webdriver
    export PATH="${PROJECT_ROOT}/resources/geckodriver/OSX:${PATH}"     
    export FF_BINARY="/Applications/Firefox.app/Contents/MacOS/firefox-bin"
fi

