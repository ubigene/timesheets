# -*- coding: utf-8 -*-
import logging
import calendar
from datetime import date

from timesheets import Timesheets


log = logging.getLogger(__name__)


def parse_holidays(holidays):
    """ Parse holidays input into a list """
    assert isinstance(holidays, list)
    rv = []
    for holiday in holidays:
        try:
            rv.append(int(holiday))
        except ValueError:
            days = holiday.split('-')
            assert len(days) == 2
            rv += range(int(days[0]), int(days[1]) + 1)
    return rv


def help_msg(name=None):
    return '''usage: timesheets.sh
            [-h]
            [-y YEAR]
            -m {January,February,March,April,May,June,July,August,September,October,November,December}
            [-l [HOLIDAYS [HOLIDAYS ...]]]

            For example, to generate timesheets for August of the current year, run:
            timesheets.sh -m August

            To generate timesheets for July 2016, assuming being on holiday from 04.07 to 08.07 and 19.07, run:
            timesheets.sh -m July -y 2016 -l 04-08 19
        '''


def populate_arg_parser(parser):
    """ Argument parser specification for o2crawler
    """
    subparsers = parser.add_subparsers(help='Commands')

    save_timesheets = subparsers.add_parser(
        "write",
        help="Save timesheet to file", usage=help_msg())
    save_timesheets.set_defaults(_run_timesheets_mode_="save_timesheets")
    save_timesheets.add_argument(
        '-y', "--year", type=int,
        help="Year", default=date.today().year)
    save_timesheets.add_argument(
        '-m', "--month", type=str,
        help="Month", required=True, choices=calendar.month_name[1:])
    save_timesheets.add_argument(
        '-l', '--holidays', nargs='*', help='Holidays')


def run(args, unknown_arg=None):
    """ Command line interface implementation """
    """ Main UI handler """
    if args._run_timesheets_mode_ == "save_timesheets":
        if args.holidays:
            holidays = parse_holidays(args.holidays)
        else:
            holidays = None
        Timesheets(args.year, args.month, holidays).save()
    else:
        raise ValueError(
            "Mode {} not supported".format(args._run_timesheets_mode_))
