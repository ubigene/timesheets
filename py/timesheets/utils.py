from openpyxl.styles.borders import Border, Side
from collections import namedtuple

Cell = namedtuple('Cell', ['row', 'column'])


def set_border(sheet, start, end, style='thin'):
    """ Draw border around a supplied openpyxl cell range """
    styles = ['thin', 'medium', 'thick']
    assert style in styles
    side = Side(border_style=style, color="FF000000")
    for row in range(start.row, end.row + 1):
        for column in range(start.column, end.column + 1):
            cell = sheet.cell(
                row=row,
                column=column)
            border = Border(
                left=cell.border.left,
                right=cell.border.right,
                top=cell.border.top,
                bottom=cell.border.bottom
            )
            if column == start.column:
                border.left = side
            if column == end.column:
                border.right = side
            if row == start.row:
                border.top = side
            if row == end.row:
                border.bottom = side
            # set new border only if it's one of the edge cells
            if (column == start.column or column == end.column or
                    row == start.row or row == end.row):
                cell.border = border
