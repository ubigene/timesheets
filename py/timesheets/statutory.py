import urllib2
import json


def init():
    response = urllib2.urlopen('https://www.gov.uk/bank-holidays.json')
    all_holidays = json.loads(response.read())
    # England and Wales only
    holidays = all_holidays['england-and-wales']['events']
    rv = {}
    for h in holidays:
        rv[h['date']] = h['title']
    return rv

HOLIDAYS = init()


def is_statutory_holiday(year, month, day):
    datestr = u"{}-{:0>2}-{:0>2}".format(year, month, day)
    assert len(datestr) == 10, datestr
    return datestr in HOLIDAYS
