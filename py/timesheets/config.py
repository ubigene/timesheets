""" Timesheets configuration file """

# Your name
NAME = "Eugene Butan"

# List of projects that you are working on
PROJECTS = [
    'ENCORE MX01850N',
    'EU Innovate MX01814N WP5']

# Schedule for the week. For each day,
# specify (Project ID, number of hours).
# Multiple projects per day are allowed
SCHEDULE = [
    [   # Monday
        ('ENCORE MX01850N', 6.75),
        ('EU Innovate MX01814N WP5', 1.25)],
    [   # Tuesday
        ('ENCORE MX01850N', 6.75)],
    [   # Wednesday
        ('EU Innovate MX01814N WP5', 7.5)],
    [   # Thursday
        ('EU Innovate MX01814N WP5', 7.5)],
    [   # Friday
        ('EU Innovate MX01814N WP5', 7.5)]]
