from openpyxl.styles import Alignment, Font
from openpyxl.utils import get_column_letter
import calendar
import openpyxl
import os
import config

from utils import Cell, set_border
from statutory import is_statutory_holiday


class Timesheets(object):
    NAME = config.NAME
    PROJECTS = config.PROJECTS
    SCHEDULE = config.SCHEDULE

    def __init__(self, year, month, holidays=None):
        self.infile = os.path.join(
            os.environ['PROJECT_ROOT'], 'resources/FEC Timesheets.xlsx')
        # month number {1 .. 12}
        self.month_idx = tuple(calendar.month_name).index(month)
        self.month = month
        self.year = year
        self.holidays = holidays

        out_filename = 'FEC Timesheets {}{:0>2}.xlsx'.format(
            self.year, self.month_idx)
        self.outfile = os.path.abspath(out_filename)

        self.book = openpyxl.Workbook()
        self.sheet = self.book.active
        self.columns = {
            'sickness': 2 + len(self.PROJECTS),
            'statutory_holiday': 2 + len(self.PROJECTS) + 1,
            'holiday': 2 + len(self.PROJECTS) + 2,
            'total': 2 + len(self.PROJECTS) + 3,
        }
        self._fill_template()
        self._draw_borders()
        self._fill_data()

    def _fill_template(self):
        """ Draw timesheets spreadsheet template """
        # Name
        self.sheet.cell(row=2, column=1).value = "Name"
        for i in range(39):
            self.sheet.cell(row=1 + i, column=1).font = Font(bold=True)
        for i in range(self.columns['total']):
            self.sheet.cell(row=3, column=1 + i).font = Font(bold=True)

        self.sheet.merge_cells(
            start_row=2,
            start_column=2,
            end_row=2,
            end_column=self.columns['total'])

        self.sheet.cell(
            row=2, column=2).value = self.NAME
        # Projects
        self.sheet.merge_cells(
            start_row=3,
            start_column=2,
            end_row=3,
            end_column=self.columns['sickness'] - 1)
        self.sheet.cell(
            row=3, column=2).value = "Research & consultancy"
        # Month
        self.sheet.cell(
                row=1, column=self.columns['holiday']).value = "MONTH:"
        self.sheet.cell(
                row=1, column=self.columns['holiday']).font = Font(bold=True)

        # Other columns (sickness, statutory holiday, holiday, total)
        self.sheet.cell(
            row=3, column=self.columns['sickness']).value = "Other - sickness"
        self.sheet.cell(
            row=4, column=self.columns['sickness']).value = "Sickness"
        self.sheet.cell(
                row=3, column=self.columns['statutory_holiday']
            ).value = "Other - statutory holiday"
        self.sheet.cell(
                row=4, column=self.columns['statutory_holiday']
            ).value = "Statutory holiday"
        self.sheet.cell(
                row=3, column=self.columns['holiday']
            ).value = "Other - holiday, etc."
        self.sheet.cell(
                row=4, column=self.columns['holiday']
            ).value = "Holiday"
        self.sheet.cell(
            row=3, column=self.columns['total']).value = "Total hours"
        self.sheet.row_dimensions[3].font = Font(bold=True)
        # 4th row
        self.sheet.cell(
            row=4, column=1).value = "Jobcode"
        for column, project in enumerate(self.PROJECTS):
            self.sheet.cell(
                row=4, column=2 + column).value = project

        for row in range(4):
            for column in range(self.columns['total']):
                self.sheet.cell(
                    row=1+row,
                    column=1+column).alignment = Alignment(wrapText=True)

        self.sheet.cell(row=37, column=1).value = "Total"
        self.sheet.cell(row=38, column=2).value = "Signed:"
        self.sheet.cell(
            row=38,
            column=self.columns['statutory_holiday']).value = "Authorised:"

        self.sheet.cell(row=5, column=1).value = "Date"
        for i in range(1, 31 + 1):
            self.sheet.cell(row=5 + i, column=1).value = i

        # row totals
        for i in range(1, 31 + 2):
            self.sheet.cell(
                    row=5 + i,
                    column=self.columns['total']
                ).value = "=SUM(B{row}:{column}{row})".format(
                    row=5 + i,
                    column=get_column_letter(self.columns['holiday']))

        # column totals
        for i in range(2, self.columns['total']):
            self.sheet.cell(
                row=37, column=i).value = "=SUM({column}6:{column}36)".format(
                    column=get_column_letter(i))

    def _draw_borders(self):
        set_border(self.sheet, Cell(1, 1),
                   Cell(39, self.columns['total']), 'medium')
        set_border(self.sheet, Cell(2, 1), Cell(2, 1), 'medium')
        set_border(self.sheet, Cell(2, 2),
                   Cell(2, self.columns['total']), 'medium')
        set_border(self.sheet, Cell(37, 2),
                   Cell(37, self.columns['total']), 'medium')

        for column in range(1, self.columns['total'] + 1):
            set_border(self.sheet, Cell(4, column), Cell(4, column), 'medium')
        for column in range(2, self.columns['total'] + 1):
            set_border(self.sheet, Cell(4, column), Cell(37, column), 'medium')

        set_border(self.sheet, Cell(37, 1), Cell(37, 1), 'medium')
        set_border(self.sheet, Cell(38, 2), Cell(39, 2), 'medium')
        set_border(self.sheet, Cell(38, self.columns['statutory_holiday']),
                   Cell(39, self.columns['statutory_holiday']), 'medium')

    def _fill_data(self):
        cal = calendar.Calendar()       # week starts on Monday by default
        for day, weekday in cal.itermonthdays2(self.year, self.month_idx):
            # for all work (Mon-Fri) days; month starts with 1
            if day > 0 and weekday < 5:
                total_day_hours = 0
                count_as_absence = False
                for project_id, project_hours in self.SCHEDULE[weekday]:
                    assert project_id in self.PROJECTS
                    column = 2 + self.PROJECTS.index(project_id)
                    total_day_hours += project_hours
                    # move time for holidays into an appropriate column
                    if self.holidays and day in self.holidays:
                        column = self.columns['holiday']
                        count_as_absence = True
                    elif is_statutory_holiday(self.year, self.month_idx, day):
                        column = self.columns['statutory_holiday']
                        count_as_absence = True
                    if not count_as_absence:
                        self.sheet.cell(
                            row=5 + day,
                            column=column).value = project_hours
                if count_as_absence:
                    self.sheet.cell(
                        row=5 + day,
                        column=column).value = total_day_hours

        # Output month for which we are doing the timesheet
        month_cell = Cell(1, 7)
        self.sheet.cell(
            row=month_cell.row, column=month_cell.column).value = self.month

    def save(self):
        self.book.save(self.outfile)
        print "Saved timesheets into {}".format(self.outfile)
