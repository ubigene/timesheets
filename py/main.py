import argparse
import py
import logging


def populate_arg_parser(parser):
    subparsers = parser.add_subparsers(help='Functions')
    for (name, help, module) in [
        ("timesheets", "Timesheets console interface", py.timesheets.main),
    ]:
        sub = subparsers.add_parser(name, help=help)
        sub.set_defaults(__bin_processor__=module.run)
        module.populate_arg_parser(sub)
    return parser


def main(args):
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='User interface scripts')
    populate_arg_parser(parser)
    args, unknown_arg = parser.parse_known_args(args)

    callFn = args.__bin_processor__
    return callFn(args, unknown_arg)
